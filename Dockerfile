FROM openjdk:11.0
ADD target/mstp-backend-0.0.1-SNAPSHOT.jar ./app.jar
ENV MONGODB_URI=mongodb://tradedb:27017/trade
ENV USER_REST=http://tradeuser:8222
ENV DATA_REST=http://tradedata:8333
EXPOSE 8080
ENTRYPOINT ["java","-jar","./app.jar"]
