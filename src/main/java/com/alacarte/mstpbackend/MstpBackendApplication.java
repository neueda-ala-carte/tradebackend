package com.alacarte.mstpbackend;

import com.alacarte.mstpbackend.utils.Authenticator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class MstpBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MstpBackendApplication.class, args);
    }

}
