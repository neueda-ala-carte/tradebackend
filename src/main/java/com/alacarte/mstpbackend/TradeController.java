package com.alacarte.mstpbackend;

import com.alacarte.mstpbackend.trade.Trade;
import com.alacarte.mstpbackend.trade.TradeType;
import com.alacarte.mstpbackend.utils.Authenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.function.Function;

@RestController
@CrossOrigin(origins = "*")
public class TradeController {

    @Autowired
    private TradeService tradeService;

    @Autowired
    private Authenticator authenticator;

    private ResponseEntity getRequestWrapper(Function<String, List> function, String apiKey) {
        String userId = authenticator.authenticate(apiKey);
        if (userId == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } else {
            List result = function.apply(userId);
            return ResponseEntity.ok().body(result);
        }
    }

    //Buy or Sell a trade
    @PostMapping("/createTrade")
    ResponseEntity create(@RequestParam String ticker, @RequestParam TradeType type,
                          @RequestParam int quantity, @RequestHeader(name = "x-api-key") String userApi){
        String userId = authenticator.authenticate(userApi);
        if (userId == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } else {
            Trade trade = tradeService.create(ticker, type, quantity, userId);
            return ResponseEntity.ok().body(trade);
        }
    }

    //Get trade history
    @GetMapping("/getHistory")
    public ResponseEntity getHistory(@RequestHeader(name = "x-api-key") String userApi,
                                     @RequestParam(required = false) Date from,
                                     @RequestParam(required = false) Date to,
                                     @RequestParam(required = false) String ticker) {
        Function<String, List> function = (userId) -> tradeService.getHistory(userId, ticker, from, to);
        return getRequestWrapper(function, userApi);
    }

    //Retrieve summary of stocks the user possesses
    @RequestMapping("/getHoldings")
    public ResponseEntity getHoldings(@RequestHeader(name = "x-api-key") String userApi,
                                      @RequestParam(required = false) String ticker) {
        Function<String, List> function = (userId) -> tradeService.getHoldings(userId, ticker);
        return getRequestWrapper(function, userApi);
    }

    //Retrieve summary of stocks the user possesses
    @RequestMapping("/getTickers")
    public ResponseEntity getHoldings(@RequestHeader(name = "x-api-key") String userApi) {
        Function<String, List> function = (userId) -> tradeService.getUserTickers(userId);
        return getRequestWrapper(function, userApi);
    }

}

