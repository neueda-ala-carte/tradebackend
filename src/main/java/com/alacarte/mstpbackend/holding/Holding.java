package com.alacarte.mstpbackend.holding;

import com.alacarte.mstpbackend.trade.TradeState;
import com.alacarte.mstpbackend.trade.TradeType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
public class Holding {

    @Id
    @JsonIgnore
    private String id;
    private String ticker;
    private String companyName;
    private int bought;
    private int sold;
    @JsonIgnore
    private String userId;

    public Holding() {}

    public Holding(String ticker, String companyName, TradeType type, int quantity, String userId) {
        this.userId = userId;
        this.ticker = ticker;
        this.companyName = companyName;
        if (type == TradeType.BUY) {
            bought = quantity;
        }
    }

}
