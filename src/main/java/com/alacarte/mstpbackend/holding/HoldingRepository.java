package com.alacarte.mstpbackend.holding;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HoldingRepository extends MongoRepository<Holding, ObjectId> {
    List<Holding> findAllByUserId(String userId);
    List<Holding> findAllByUserIdAndTicker(String userId, String ticker);

}




