package com.alacarte.mstpbackend.trade;

import com.alacarte.mstpbackend.trade.Trade;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Repository
public interface TradeRepository extends MongoRepository<Trade, ObjectId> {

    List<Trade> findAllByUserId(String userId);
    List<Trade> findAllByUserIdAndTicker(String userId, String ticker);
    List<Trade> findAllByUserIdAndCreatedBetween(String userId, Date from, Date to);
    List<Trade> findAllByUserIdAndTickerAndCreatedBetween(String userId, String ticker, Date from, Date to);

}
