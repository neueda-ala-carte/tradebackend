package com.alacarte.mstpbackend.trade;

public enum TradeState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String state;

    TradeState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }
}
